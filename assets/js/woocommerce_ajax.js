/**
  * here add scripts and code ajax   
  * alzohor_action_ajax
  */



jQuery('.add-cart').click(function(e){
     e.preventDefault();
     jQuery(this).children('.lds-roller').show();
      var id = $(this).val();
      var img_product = jQuery(this).attr('data-image') || 0;
      var price_product = jQuery(this).attr('data-price') || 0;
      var title_product = jQuery(this).attr('data-title') || 0;
      var quantity_value = $('input[name=quantity]').val() || 1;
      var product_qty = jQuery(this).attr('data-quantity') || quantity_value;
      var product_id = jQuery(this).attr('data-id') || id;
      var variation_id = jQuery('input[name=variation_id]').val() || 0;
      var data = {
            action: 'woocommerce_ajax_add_to_cart',
            product_id: product_id,
            product_sku: '',
            quantity: product_qty,
            variation_id:variation_id
        };

     jQuery.ajax({
        type:'POST',
        url :alzohor_action_ajax.ajaxurl,
        data:data,
        success:function(result){
            console.log(result);
            jQuery('.lds-roller').hide();
            jQuery('.counter-products-in-cart').load(' .counter-products-in-cart>* ');
            jQuery('.search-box').load(' .search-box>* ');
           // if( (result!='') || result != null) ){
                jQuery('.container-alert-add-to-cart').html('<div class="alert-add-to-cart"><div class="close_alert_cart"><i class="fa fa-remove"></i></div><div class="img-alert-cart"><img src="'+img_product+'" ></div><div class="item-cart-details-alert-cart"><div class="title-alert-cart"><h4>'+title_product+'</h4></div><div class="amount-price-alert-cart"><label>'+quantity_value+' * '+price_product+'</label></div></div></div>');
                jQuery('.alert-add-to-cart').fadeIn(1000);
                setTimeout(function(){
                 jQuery('.alert-add-to-cart').fadeOut(1000);
                },2000);
           // }
        }


     });
});


jQuery('.mini_cart_item > a').click(function(e){
     e.preventDefault();
     var product_id = $(this).attr('data-product_id');
     var key_product= $(this).attr('data-cart_item_key');
     var data = {
            action: 'woocommerce_ajax_remove_item_from_cart',
            product_id: product_id,
            cart_item_key: key_product,
        };
    jQuery.ajax({
        type:'POST',
        url :alzohor_action_ajax.ajaxurl,
        data:data,
        success:function(result){
            console.log(result);
            jQuery('.lds-roller').hide();
            jQuery('.counter-products-in-cart').load(' .counter-products-in-cart>* ');
            jQuery('.search-box').load(' .search-box>* ');
            
        }


    });

});




jQuery('.add_to_favorite').click(function(e){
     e.preventDefault();
     jQuery(this).css({'color':'red'});
     var product_id = $(this).attr('data-product_id');
     var user_id= $(this).attr('data-user_id');
     var data = {
            action: 'woocommerce_ajax_add_to_favorite',
            product_id: product_id,
            user_id: user_id,
        };
    jQuery.ajax({
        type:'POST',
        url :alzohor_action_ajax.ajaxurl,
        data:data,
        success:function(result){
            console.log(result);
           if( (result!='no')){
              window.location.href=result;
            }

           /* jQuery('.lds-roller').hide();
            jQuery('.counter-products-in-cart').load(' .counter-products-in-cart>* ');
            jQuery('.search-box').load(' .search-box>* ');*/
            
        }


    });

});






