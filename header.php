<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title> متجر الزهور </title>
    <meta name="description" content=" متجر الزهور">
    <meta name="author" content="متجر الزهور">
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Put favicon.ico and apple-touch-icon(s).png in the images folder -->
    <link rel="shortcut icon" href="icon.png">
    <?php wp_head(); ?>
</head>
<body>
    <div class="container-alert-add-to-cart">
        
    </div>
    <div class="pageWrapper">
        <!-- login box start -->
        <!-- login box End -->
        <!-- Header Start -->
        <div id="headWrapper" class="clearfix">
            <!-- Logo, global navigation menu and search start -->
            <header class="top-head" data-sticky="true">
                <div class="container">
                    <div class="row">
                        <div class="logo cell-3">
                            <?php $link_logo = get_template_directory_uri().'/assets/images/rode-flower-logo-8F2FDDC77A-seeklogo.com.png'; ?>
                           <img width="100px" src="<?php echo $link_logo; ?>" />
                        </div>
                        <div class="cell-9 top-menu">
                            <!-- top navigation menu start -->
                            <nav class="top-nav mega-menu">
                                <?php 
                                     /**
                                       * show header menus                                      *
                                       **/ 
                                        show_header_menu();
                                ?>
                            </nav>
                            <!-- top navigation menu end -->
                            <!-- top search start -->
                            <div class="icons_header">
                                <a href="<?php echo get_page_link(get_option( 'woocommerce_myaccount_page_id' )); ?>">
                                    <span class="fa fa-user-o" aria-hidden="true"></span>
                                </a>
                                <a class="" href="<?php echo get_page_link(get_page_by_title('favorites') ); ?>" >
                                   <span class="fa fa-heart-o" aria-hidden="true"></span>
                                </a>
                                
                            </div>

                            <div class="top-search">
                                <?php //if( (!empty(WC()->cart->get_cart_contents_count())) || (WC()->cart->get_cart_contents_count()!=0 ) ): ?>
                                     <div class="counter-products-in-cart"><label><?php echo WC()->cart->get_cart_contents_count(); ?></label></div>
                                <?php //endif; ?>
                                <a href="#">
                                    <!-- <span class="fa fa-shopping-cart"></span> -->
                                    <span class="fa fa-shopping-bag" aria-hidden="true"></span>
                                </a>
                                
                                <div class="search-box">
                                    <?php  woocommerce_mini_cart();  ?>
                                </div>
                            </div>

                            <!-- top search end -->
                        </div>
                    </div>
                </div>
            </header>
            <!-- Logo, Global navigation menu and search end -->
        </div>
        <!-- Header End -->

       

