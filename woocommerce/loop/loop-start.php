<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<!-- <ul class="products columns-<?php //echo esc_attr( wc_get_loop_prop( 'columns' ) ); ?>"> -->
<?php if(is_front_page()): ?>
<!--  <div class="sectionWrapper label-section2">
    <div class="container">
        <h2><span>منتاجتنا</span></h2>
        <br><br> -->
        <div class="portfolio-box">
            <div class="portfolio-filterable">
                <div class="row">
                    <div class="skew-0">
                        <ul id="filters">
                            <li class="active">
                                <a href="#" class="skew-0 filter" data-filter="*"> الكل </a>
                            </li>
                            <?php 
                                global $product;
                                $categories = get_terms( array('taxonomy'=>'product_cat') );
								$all_category='';
								//var_dump($categories);
								foreach ($categories as $category) { ?>
									   <li>  <a href="#" class="skew-0 filter" data-filter="<?php echo (!empty($category->name)?'.'.$category->name:''); ?> "> <?php echo (!empty($category->name)?$category->name:''); ?>  </a>  </li>
							<?php	} ?>
                        </ul>
                    </div>
                    <div class="products portfolio-items columns-<?php echo esc_attr( wc_get_loop_prop( 'columns' ) ); ?> moh-all " id="container">

<?php    endif; ?>

  