    
            </div>
        </div>
    </div>
        <!-- Footer start -->
        <footer id="footWrapper">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="cell-6">
                            <div class="foot-logo">
                                <p>
                                    LOGO
                                </p>
                            </div>
                            <p class="no-margin"> وصف للشركة هنا وصف للشركة هنا وصف للشركة هنا وصف للشركة هنا وصف للشركة هنا وصف للشركة هنا </p>
                            <form action="link-to-your-site" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank"
                                novalidate="" class="NL">
                                <div class="skew-0 input-box left">
                                    <input type="email" value="" name="EMAIL" class="txt-box" id="mce-EMAIL" placeholder="ادخل ايميلك " required="">
                                </div>
                                <div class="left skew-0 NL-btn">
                                    <input type="submit" value="ارسال" name="subscribe" id="mc-embedded-subscribe" class="btn">
                                </div>
                                <div class="hidden">
                                    <input type="text" name="name-of-the-hidden-field" value="">
                                </div>
                                <div class="Notfication fx animated fadeInRight undefined">
                                    <a class="close-box" href="#">
                                        <i class="fa fa-times"></i>
                                    </a>
                                    <p></p>
                                </div>
                            </form>
                        </div>
                        <!-- main menu footer cell start -->
                        <div class="cell-3">
                            <h3 class="block-head">روابط تهمك </h3>
                            <!-- <ul class="footer-menu">
                                <li>
                                    <a href="index.html"> الرئيسية </a>
                                </li>
                                <li>
                                    <a href=""> الرئيسية </a>
                                </li>
                                <li>
                                    <a href=""> الرئيسية


                                    </a>
                                </li>
                                <li>
                                    <a href=""> الرئيسية </a>
                                </li>
                                <li>
                                    <a href=""> اتصل بنا </a>
                                </li>
                            </ul> -->
                        </div>
                        <div class="cell-3 f-text">
                            <h3 class="block-head"> اتصل بنا </h3>
                            <ul>
                                <li class="footer-contact">
                                    <span> العنوان : عنوان هنا </span>
                                </li>
                                <li class="footer-contact">
                                    <span>
                                        <a href="#"> info@test.com </a>
                                    </span>
                                </li>
                                <li class="footer-contact">
                                    <span>+524546 : جوال </span>
                                </li>
                                <li class="footer-contact">
                                    </i>
                                    <span> 011 / 2455533 : هاتف </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer bottom bar start -->
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <!-- footer copyrights left cell -->
                        <div class="copyrights cell-5"> جميع الحقوق محفوظة &copy; لشركة متجر الزهور 2016 </div>
                        <!-- footer social links right cell start -->
                        <div class="cell-7">
                            <ul class="social-list right">

                                <li class="skew-0">
                                    <a href="https://twitter.com/" data-title="twitter" data-tooltip="true" target="_blank">
                                        <span class="fa fa-twitter skew0"></span>
                                    </a>
                                </li>
                                <li class="skew-0">
                                    <a href=" https://www.instagram.com//" data-title="instagram" data-tooltip="true" target="_blank">
                                        <span class="fa fa-instagram"></span>
                                    </a>
                                </li>
                                <li class="skew-0">
                                    <a href=" https://www.instagram.com//" data-title="instagram" data-tooltip="true" target="_blank">
                                        <span class="fa fa-snapchat"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- footer social links right cell end -->
                    </div>
                </div>
            </div>
            <!-- footer bottom bar end -->
        </footer>
        <!-- Footer end -->
        <!-- Back to top Link -->
        <div id="to-top" class="main-bg">
            <span class="fa fa-chevron-up"></span>
        </div>
    </div>
    <?php wp_footer(); ?>

  

</body>