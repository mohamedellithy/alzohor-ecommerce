<?php
/**
 * elzehore theme options install menus in theme class class_install_menus_theme()
 * @package WordPress
 * @subpackage alzehor
 * @since 1.0
 *
 **/ 
class wp_install_menus_theme{
     
     function __construct(){
       add_action('init',array($this,'install_header_menu'));
       $this->install_database_favorites();
     }

     function install_header_menu(){

     	register_nav_menus(
     		array(
 	 	  	     'header-alzohor-menu'=>'header alzohor menu',
 	 	  	     'footer-alzohor-menu'=>'footer alzohor menu',
     	));
     }

     function install_database_favorites(){
     	  global $wpdb;
    
            $table_name = $wpdb->prefix . "_favorites";
    
		    if($wpdb->get_var(" SHOW TABLES LIKE '$table_name'") != $table_name ){
		            $charset_collate = $wpdb->get_charset_collate();
		        
		            $sql = "CREATE TABLE $table_name (
		              id mediumint(9) NOT NULL AUTO_INCREMENT,
		              user_id int(255) NOT NULL ,
		              product_id int(255) NOT NULL ,
		              PRIMARY KEY  (id)
		            ) $charset_collate;";
		            
		            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		            
		            dbDelta( $sql );
		    }
     }

}