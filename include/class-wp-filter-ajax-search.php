<?php 
class wp_filter_ajax_search{
	
	public $Categories;
	public $low_price;
	public $hight_price;
	public $taste;
	public $order_by;
	public $count_products;
	public $order_products_arrange;
	public $link=null;
	public $all_attrs_link;

	function __construct(){
          $this->fixed_search_engin();
          $this->filter_search_action();
	}

	function filter_search_action(){
        $this->link=get_permalink( wc_get_page_id( 'shop' ) ).'?type=search_filter';
		if(!empty($_GET)){
			$all_attrs_link=$_GET;
			/*$this->link='?';
			foreach ($_GET as $key => $value) {
				$this->link.=$key.'='.$value.'&';
			}*/
		}
		if(isset($_POST['search_filter'])){
            if( empty($_POST) || !wp_verify_nonce($_POST['search_filter_price'],'search_filter_price_field') ){
            	exit;
            }
            $Category_for_product=$_POST['product_Category'];
            $low_price=$_POST['low_price'];
            $hight_price=$_POST['hight_price'];
            $taste=$_POST['checkbox_input'];

            if(!empty($Category_for_product)){
            	$this->Categories = $Category_for_product;
            	//$this->link.='&Category='.$this->Categories;
            	$all_attrs_link['Category']=$this->Categories;
            	add_action( 'woocommerce_product_query' , array($this,'custom_search_query_category'),10,1);
            }  

            if( !empty($low_price) || !empty($hight_price)  ) {
            	$this->hight_price = ( (!empty($hight_price) )?$hight_price:'10000');
            	$this->low_price   = ( (!empty($low_price) )?$low_price:'0');
            	$all_attrs_link['low_price']=$this->low_price;
            	$all_attrs_link['hight_price']=$this->hight_price;
            	/*$this->link.='&low_price='.$this->low_price;
            	$this->link.='&hight_price='.$this->hight_price;*/
            	add_action( 'woocommerce_product_query' , array($this,'custom_search_query_price'),10,1);
            }  

            if( !empty($taste) ){
            	$this->taste = $taste;
            	$all_attrs_link['taste']=implode('-', $this->taste);
            	//$this->link.='&taste='.implode('-', $this->taste);
            	add_action( 'woocommerce_product_query' , array($this,'custom_search_query_taste'),10,1);
            }
            
            foreach ($all_attrs_link as $key => $value) {
            	if($key!='type'){
				    $this->link.='&'.$key.'='.$value;
            	}
			}
            wp_redirect($this->link);

            exit;
		}
        
        /**
         * request method post of order
         **/
		if( ($_SERVER['REQUEST_METHOD'] == 'POST') ){
              $this->order_by =$_POST['order_by'];
                if(!empty($this->order_by)){
                	  $all_attrs_link['order_by']=$this->order_by;             
		              add_action( 'woocommerce_product_query' , array($this,'custom_search_query_order_by'),10,1);
                       foreach ($all_attrs_link as $key => $value) {
			            	if($key!='type'){
							    $this->link.='&'.$key.'='.$value;
			            	}
						}
		              wp_redirect($this->link);
		              exit;
	            }
		}


		/**
         * request method count posts in show
         **/
		if( ($_SERVER['REQUEST_METHOD'] == 'POST') ){
              $this->count_products =$_POST['number_product'];
                if(!empty($this->count_products)){
                	 $all_attrs_link['number_product']=$this->count_products;
		              add_filter( 'loop_shop_per_page', array($this,'custom_search_query_count_product') ,10,1);
		                foreach ($all_attrs_link as $key => $value) {
			            	if($key!='type'){
							    $this->link.='&'.$key.'='.$value;
			            	}
						}
		              wp_redirect($this->link);
		              exit;
	            }
		}



		/**
         * request method order posts asc or desc in show
         **/
		if( ($_SERVER['REQUEST_METHOD'] == 'POST') ){
              $this->order_products_arrange =$_POST['order_product_arrange'];
                if(!empty($this->order_products_arrange)){
                	 $all_attrs_link['order']=$this->order_products_arrange;
		              add_action( 'woocommerce_product_query', array($this,'custom_search_query_order_product') ,10,1);
		                foreach ($all_attrs_link as $key => $value) {
			            	if($key!='type'){
							    $this->link.='&'.$key.'='.$value;
			            	}
						}
		              wp_redirect($this->link);
		              exit;
	            }
		}


	}

	function fixed_search_engin(){
        if(!empty($_GET['type'])){
	        $this->taste          =explode('-',esc_attr($_GET['taste']) );
	        $this->low_price      =esc_attr($_GET['low_price']);
	        $this->hight_price    =esc_attr($_GET['hight_price']);
	        $this->Categories     =esc_attr($_GET['Category']);
	        $this->order_by       =esc_attr($_GET['order_by']);
	        $this->count_products =esc_attr($_GET['number_product']);
	        $this->order_products_arrange = esc_attr($_GET['order']);
	        array_shift($this->taste);
	        if(!empty($this->taste)){
				add_action( 'woocommerce_product_query' , array($this,'custom_search_query_taste'),10,1);
	        }
	         
	        if( !empty($this->low_price) || !empty($this->hight_price) ){
				add_action( 'woocommerce_product_query' , array($this,'custom_search_query_price'),10,1);
	        }
	        if(!empty($this->Categories)){
	        	 add_action( 'woocommerce_product_query' , array($this,'custom_search_query_category'),10,1);
	        }
	        if(!empty($this->order_by)){
	        	 add_action( 'woocommerce_product_query' , array($this,'custom_search_query_order_by'),10,1);
	        }
	        if(!empty($this->count_products)){
	        	if($this->count_products=='all'){
	        		$this->count_products=-1;
	        	}
	        	 add_filter( 'loop_shop_per_page' , array($this,'custom_search_query_count_product'),10,1);
	        }

	        if(!empty($this->order_products_arrange)){
	        	 add_action( 'woocommerce_product_query' , array($this,'custom_search_query_order_product'),10,1);
	        }
	    }
       
 	}


	function custom_search_query_category($q){
		$tax_query = $q->get( 'tax_query' );        
        $tax_query[]=array(
            'tax_query' => array( // NOTE: array of arrays!
	            array(
		            'taxonomy' => 'product_cat',
		            'field'    => 'name',
		            'terms'    => $this->Categories
	            )
            )
        );
	    $q->set( 'tax_query', $tax_query );
	}

	function custom_search_query_price($q){
	    $meta_query=$q->get('meta_query');
	    $meta_query[] = array(
            'key'    => '_regular_price',
            'value'  => array(
            	    $this->low_price,
            	    $this->hight_price
                ),
            'compare'=> 'BETWEEN',
            'type'   =>'NUMERIC'
	    );
	    $q->set('meta_query',$meta_query);
 	}

 	function custom_search_query_taste($q){
 		$tax_query = $q->get( 'tax_query' );        
        $tax_query[]=array(
            'tax_query' => array( // NOTE: array of arrays!
	            array(
		            'taxonomy' => 'pa_taste',
		            'field'    => 'name',
		            'terms'    => $this->taste
	            )
            )
        );
	    $q->set( 'tax_query', $tax_query );

 	}

 	function custom_search_query_order_by($q){
 		  if( $this->order_by == 'date'){
            $order_by='date';
            $meta_key='';
 		  }
 		  elseif( $this->order_by == 'price'){
            $order_by='meta_value_num';
            $meta_key='_price';
 		  }
 		  elseif(  $this->order_by == 'all'){
            $order_by='date meta_value_num';
            $meta_key='_price';
 		  }

          $q->set( 'orderby',  $order_by );
         
          $q->set( 'meta_key', $meta_key );
 	}

 	function custom_search_query_order_product($q){
 		 $q->set( 'order',$this->order_products_arrange);
 	}


 	function custom_search_query_count_product($cols){
        return $this->count_products;
 	}


 	


}

