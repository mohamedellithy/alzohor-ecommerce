<?php  

   add_action( 'woocommerce_widget_shopping_cart_buttons', function(){
    // Removing Buttons
    remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_button_view_cart', 10 );
    remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout', 20 );

    // Adding customized Buttons
    add_action( 'woocommerce_widget_shopping_cart_buttons', 'custom_widget_shopping_cart_button_view_cart', 10 );
    add_action( 'woocommerce_widget_shopping_cart_buttons', 'custom_widget_shopping_cart_proceed_to_checkout', 20 );
}, 1 );

// Custom cart button
function custom_widget_shopping_cart_button_view_cart() {
    $original_link = wc_get_cart_url();
    $custom_link = home_url( '/cart/?id=1' ); // HERE replacing cart link
    echo '<a href="' . esc_url( $custom_link ) . '" class="button wc-forward">' . esc_html__( 'الاطلاع على السلة', 'woocommerce' ) . '</a>';
}

// Custom Checkout button
function custom_widget_shopping_cart_proceed_to_checkout() {
    $original_link = wc_get_checkout_url();
    $custom_link =  wc_get_checkout_url(); // HERE replacing checkout link
    echo '<a href="' . esc_url( $custom_link ) . '" class="button checkout wc-forward">' . esc_html__( 'الدفع', 'woocommerce' ) . '</a>';
    ?>
      <script type="text/javascript">
             $('a.wc-forward ').click(function(e){
		          e.delegateEvents();
	         });
      </script>
    <?php 
}


add_action('woocommerce_before_cart','show_before_cart_template_page');

function show_before_cart_template_page(){
   if(!is_front_page() || !is_home() ){
        /*  Content Start   */
        echo "<div id='contentWrapper' class='contentWrappers'>";
    ?>
        <div class="page-title title-1">
            <div class="container">
                <h3 class="labelBanner">
                    <?php  echo ((is_page('cart'))?'سلة المشتريات':woocommerce_page_title()); ?>
                </h3>
            </div>
        </div>
        <div class="sectionWrapper">
            <div class="container">
    <?php } 
}



add_action('woocommerce_after_cart','show_after_cart_template_page');

function show_after_cart_template_page(){?>
        </div>
    </div>
<?php }
add_action( 'woocommerce_before_checkout_form','alzohor_before_checkout_form');
function alzohor_before_checkout_form(){ ?>
           <div class="sectionWrapper">
            <div class="container">
<?php }

add_action( 'woocommerce_after_checkout_form', 'alzohor_after_checkout_form');
function alzohor_after_checkout_form(){ ?>
        </div>
        </div>
<?php }


remove_action( 'woocommerce_cart_collaterals','woocommerce_cross_sell_display');