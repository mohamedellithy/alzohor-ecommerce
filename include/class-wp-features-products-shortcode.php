<?php 
class wp_features_products_shortcode{

	function __construct(){
         add_action('alzohor_using_slick_slider',array($this,'using_slick_slider'),10,2 );
         add_action('alzohor_show_adverisment_photo',array($this,'show_adverisment_photo'));
         add_action('alzohor_show_for_sale_products',array($this,'show_for_sale_products'));
         add_action('alzohor_show_all_favorites_products',array($this,'show_all_favorites_products') );
         
	}

	function get_all_products_for_slick($type_products='',$class_attr_image=''){
         $args=array(
		                'post_type'=>'product',
		                'posts_per_page'=>8,
		                'order'=>'asc',
		         );

        $all_products=new WP_QUERY($args);
        if($all_products->have_posts()):
        	while($all_products->have_posts()): $all_products->the_post();
              $product = new WC_Product( get_the_ID() );
        	 ?>
                 
                 <div class="img_product_slick <?php echo $class_attr_image; ?>">
                 	 <img src="<?php echo get_the_post_thumbnail_url(); ?>" />
                     <p class="title_product_slick"> <a href="<?php echo the_permalink(); ?>"> <?php echo the_title(); ?> </a> </p> 
                     <p class="price_product_slick"> <?php  wc_get_template( 'loop/price.php' ); ?> </p> 
                     <div class="options_icons">
                     	  <?php
                           $user = wp_get_current_user();  ?>
                          <i class="fa fa-heart-o icon_products add_to_favorite"  data-user_id="<?php echo $user->id; ?>" data-product_id="<?php echo $product->get_id(); ?>" aria-hidden="true"></i> 
                                
                     	 <?php wc_get_template( 'loop/add-to-cart.php'); ?>
                     </div>
                 </div>
        <?php 
            endwhile;
        endif;
        wp_reset_postdata();


	}


	function using_slick_slider($class_name,$title_section){ ?>
	    <div class="sectionWrapper label-section2">
		    <div class="container">
				<h2><span> <?php echo $title_section; ?> </span></h2>
			<!-- </div>
		</div> -->
        <div class="<?php echo $class_name; ?> div_slider_container" >
		     <?php $this->get_all_products_for_slick();  ?>
		</div>
		
<?php 

	}

	function show_adverisment_photo(){ ?>
	<div class="sectionWrapper label-section2">
        <div class="container">
        	<div class="portfolio-box">
	            <div class="portfolio-filterable">
	                <div class="row">
			        	<div class="container_image">
			        	     <img src="<?php echo get_template_directory_uri().'/assets/images/flower-store-templates.jpg'; ?>  " />
			        	</div>
			        </div>
			    </div>
			</div>
        </div>
    </div>
<?php 
	}

	function show_for_sale_products(){ ?>
        <div class="view-all-projects">
                <label class="title_section"> العروض المميزة </label>
         </div>
         <div class="sectionWrapper label-section2">
	        <div class="container">
	        	<div class="portfolio-box">
		            <div class="portfolio-filterable">
		                <div class="row">
		                	<div class="right_sale_up_to_20_perc">
		                		  <div class="heading_section_sale"> 
		                		  	   <h3> خصومات تصل الى 20 %</h3>
		                		  	   
		                		  </div>
		                		  <div class="discount_20_percent div_slider_container" >
										 <?php $this->get_all_products_for_slick('','no_radius');  ?>
								   </div>
		                	</div>
		                    <div class="left_sale_up_to_50_perc">
		                		 <div class="heading_section_sale"> 
		                		  	   <h3> خصومات تصل الى 50 %</h3>
		                		  </div>
		                		  <div class="discount_50_percent div_slider_container" >
										 <?php $this->get_all_products_for_slick('','no_radius');  ?>
								   </div>
		                	</div>
		                </div>
		            </div>
		        </div>
		     </div>
		  </div>


<?php 
	}
	function get_all_favorites_array(){
		global $wpdb;					                 
	     /** table name with prefix **/
	     $table_name = $wpdb->prefix.'_favorites';
	     $user = wp_get_current_user(); 
	     $user_id=$user->id;
	     $get_results_of_tests_for_student = $wpdb->get_results("SELECT product_id FROM $table_name WHERE user_id= $user_id ",ARRAY_A);
										    
	     return $get_results_of_tests_for_student;
	}

	function handle_get_all_favorites_array(){
         $get_results_of_tests_for_studentsd = $this->get_all_favorites_array();
         $idss=array();
         if(!empty($get_results_of_tests_for_studentsd)){
	          foreach ($get_results_of_tests_for_studentsd as $value) {
					array_push($idss,$value['product_id']);
		     }
		 }

		 return $idss;

	}

	function show_all_favorites_products(){ ?>

		<div class=" label-section2 logo_in_pages">
		    <div class="container">
		        <h2><span>المفضلة لديك </span></h2>
		    </div>
		</div>
         <div class="sectionWrapper label-section2">
	        <div class="container">
	        	<div class="portfolio-box">
		            <div class="portfolio-filterable">
		                <div class="row">
		                	<div class="container_favorites_product">

		                           <?php 
		                           if(is_user_logged_in() ){
				                             $get_results_of_tests_for_student = $this->get_all_favorites_array();
										     foreach ($get_results_of_tests_for_student as $value) {
						            				$ids.=$value['product_id'].',';
										     }
										     if(!empty($ids)){
				                           
				                                    echo do_shortcode('[products ids="'.$ids.'" ]');
				                              }
		                           	
		                           }
		                           else { ?> 

		                                     <div class='message_must_login'> قم بتسجيل الدخول الى حسابك لكى تقوم بالاطلاع على مفضلاتك</div>
		                           <?php } ?>
		                     </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>

	<?php 
	}
}