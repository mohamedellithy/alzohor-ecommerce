<?php 



class wp_widget_search_filter extends WP_Widget {

    

    function __construct(){

      	parent::__construct(

	      		// widget ID

				'alzohor_search_filter_widget',

				// widget name

				__('Alzohor search filter widget', 'alzohor'),

				// widget description

				array ( 'description' => __( 'Alzohor search filter widget', 'hstngr_widget_domain' ), )

      		);

    }



    public function widget( $args, $instance ) {

		$title  = apply_filters( 'widget_title', $instance['title'] );

		$text= apply_filters( 'widget_text', $instance['text'] );

		$search_tast= apply_filters( 'widget_search_tast', $instance['search_tast'] );

		$advanced_search = apply_filters( 'widget_advanced_search', $instance['advanced_search'] );

    ?>



        

          	<form method="POST">

          		<?php wp_nonce_field('search_filter_price_field','search_filter_price'); ?>

                <div class="widget fx" data-animate="fadeInRight">

                    <h3 class="widget-head"> <?php echo $title; ?> </h3>

                    <div class="widget-content">

                        <ul id="accordion" class="accordion">

                            <?php if(!empty($advanced_search)): ?>

                            <li>

                                <h3>

                                    <a href="#"> بحث متقدم </a>

                                </h3>

                                <div class="accordion-panel active">

                                    <div class="control-group">

                                        <label class="control-label"><?php _e('Category','woocommerce'); ?> :</label>

                                        <div class="controls">

                                            <?php do_action('show_dropdown_taxonomy_product'); ?>

                                        </div>

                                    </div>



                                    <div class="control-group">

                                        <div class="cell-6">

                                            <label class="control-label"> اقل سعر :</label>

                                            <div class="controls">

                                                <?php do_action('show_dropdown_prices_search','low_price'); ?>

                                            </div>

                                        </div>

                                        <div class="cell-6">

                                            <label class="control-label"> اكبر سعر</label>

                                            <div class="controls">

                                                <?php do_action('show_dropdown_prices_search','hight_price'); ?>

                                            </div>

                                        </div>

                                    </div>





                                </div>

                            </li>

                        <?php   endif; 

                                /**

                                * here show all taste attributes belongs products

                                **/

                                if(!empty($search_tast) ):

                                    do_action('show_dropdown_taste_search'); 

                                endif; 

                        ?>



                        </ul>

                       

                        <input name="search_filter" type="submit" class="btn btn-small main-bg" value="بحث " style="width: 100%;" />

                    </div>

                </div>

            </form>



                         

                        

    <?php 



    }



    public function form( $instance ) {

		if ( isset( $instance[ 'title' ] ) ){

		     $title = $instance[ 'title' ];

		}

		else

		{

		     $title = __( 'Default Title', 'hstngr_widget_domain' );

		}



		if ( isset( $instance[ 'text' ] ) ){

		     $text = $instance[ 'text' ];

		}

		else

		{

		     $text = __('[content-filter-search]', 'hstngr_widget_domain' );

		}



		if ( isset( $instance[ 'advanced_search' ] ) ){

		     $advanced_search = $instance[ 'advanced_search' ];

		}

		else

		{

		     $advanced_search = __('checked', 'hstngr_widget_domain' );

		}



		if ( isset( $instance[ 'search_tast' ] ) ){

		     $search_tast = $instance[ 'search_tast' ];

		}

		else

		{

		     $search_tast = __('checked', 'hstngr_widget_domain' );

		}







		?>

		<p>

		    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>

		    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />

		</p>



		<p>

		    <label for="<?php echo $this->get_field_id( 'advanced_search' ); ?>"><?php _e( ' - allow advanced search:' ); ?></label>

		    <input type="checkbox" class="widefat" id="<?php echo $this->get_field_id( 'advanced_search' ); ?>" name="<?php echo $this->get_field_name( 'advanced_search' ); ?>" <?php echo ( !empty(esc_attr( $advanced_search ) )?'checked':''); ?> />

		</p>



		<p>

		    <label for="<?php echo $this->get_field_id( 'search_tast' ); ?>"><?php _e( ' - allow search Taste:' ); ?></label>

		    <input type="checkbox" class="widefat" id="<?php echo $this->get_field_id( 'search_tast' ); ?>" name="<?php echo $this->get_field_name( 'search_tast' ); ?>" <?php echo ( !empty(esc_attr( $search_tast ) )?'checked':''); ?> />

		</p>





		<p>

		    <label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'content:' ); ?></label>

		    <input class="widefat" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>" type="text" value="<?php echo esc_attr( $text ); ?>" readonly/>

		</p>



		



		<?php

    }





    public function update( $new_instance, $old_instance ) {

		$instance = array();

		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		$instance['advanced_search'] = ( ! empty( $new_instance['advanced_search'] ) ) ? strip_tags( $new_instance['advanced_search'] ) : '';

		$instance['search_tast'] = ( ! empty( $new_instance['search_tast'] ) ) ? strip_tags( $new_instance['search_tast'] ) : '';

		$instance['text'] = ( ! empty( $new_instance['text'] ) ) ? strip_tags( $new_instance['text'] ) : '';

		return $instance;

	}









}