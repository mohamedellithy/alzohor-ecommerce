<?php
/**
 * elzehore theme options install styles and scripts in theme class class_install_styles_scripts_file_theme()
 * @package WordPress
 * @subpackage alzehor
 * @since 1.0
 *
 **/ 
class wp_install_styles_scripts_file_theme{
     
     function __construct(){

     	 add_action('wp_enqueue_scripts',array($this,'install_styles_css_theme'));

     	 add_action('wp_enqueue_scripts',array($this,'install_scripts_js_theme'));
         
     }

     function install_styles_css_theme(){
     	 global $wp_styles;

         wp_enqueue_style('alzehor_google_apis_font','https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800&amp;amp;subset=latin,latin-ext',array(),'');
         wp_enqueue_style('alzehor_fontawesome',Theme_url_directory.'/assets/css/font-awesome.min.css',array(),'');
         wp_enqueue_style('alzehor_animatecss',Theme_url_directory.'/assets/css/animate.css',array(),'');
         wp_enqueue_style('alzehor_photo_css',Theme_url_directory.'/assets/css/prettyPhoto.css',array(),'');
         wp_enqueue_style('alzehor_slick_css',Theme_url_directory.'/assets/css/slick.css',array(),'');
         wp_enqueue_style('alzehor_settings_css',Theme_url_directory.'/assets/rs-plugin/css/settings.css',array(),'');
         wp_enqueue_style('alzehor_wow_slider_css',Theme_url_directory.'/assets/css/stylewow.css',array(),'');
         wp_enqueue_style('alzehor_style_css',Theme_url_directory.'/assets/css/style_css.css',array(),'');
         wp_enqueue_style('alzehor_responsive_css',Theme_url_directory.'/assets/css/responsive.css',array(),'');
         wp_enqueue_style('alzehor_rtl_css',Theme_url_directory.'/assets/css/rtl.css',array(),'');
         wp_enqueue_style('alzehor_font_awesom','https://kit.fontawesome.com/6a0783bcfd.js',array(),'');
         wp_enqueue_style('alzehor_custom_rtl_css',Theme_url_directory.'/assets/css/custom-rtl.css',array(),'');
         wp_enqueue_style( 'my-theme-ie', Theme_url_directory. "/assets/css/ie.css", array(),'');
         $wp_styles->add_data('my-theme-ie','conditional','IE');
         wp_enqueue_style( 'alzehor_skins_default', Theme_url_directory. "/assets/css/skins/default.css", array(),'');
         

     }

     function install_scripts_js_theme(){
        global $wp_scripts;
        wp_deregister_script('jquery');
        wp_enqueue_script('alzehor_jQuery',Theme_url_directory.'/assets/js/jquery.min.js',array(),'',false);
	 	wp_enqueue_script('alzehor_waypoints',Theme_url_directory.'/assets/js/waypoints.min.js',array(),'',true);
	 	wp_enqueue_script('alzehor_themepunch_tools',Theme_url_directory.'/assets/rs-plugin/js/jquery.themepunch.tools.min.js',array(),'',true);
	    wp_enqueue_script('alzehor_themepunch_revolution',Theme_url_directory.'/assets/rs-plugin/js/jquery.themepunch.revolution.min.js',array(),'',true);
	 	wp_enqueue_script('alzehor_animateNumber',Theme_url_directory.'/assets/js/jquery.animateNumber.min.js',array(),'',true);
	 	wp_enqueue_script('alzehor_slick_min',Theme_url_directory.'/assets/js/slick.min.js',array(),'',true);
	 	wp_enqueue_script('alzehor_easypiechart_min',Theme_url_directory.'/assets/js/jquery.easypiechart.min.js',array(),'',true);
	 	wp_enqueue_script('alzehor_prettyPhoto_js',Theme_url_directory.'/assets/js/jquery.prettyPhoto.js',array(),'',true);
        /*wp_enqueue_script('alzehor_sharre_js',Theme_url_directory.'/assets/js/jquery.sharrre.min.js',array(),'',true);*/
        wp_enqueue_script('alzehor_elevateZoom_js',Theme_url_directory.'/assets/js/jquery.elevateZoom-3.0.8.min.js',array(),'',true);
	 	wp_enqueue_script('alzehor_placeholder_js',Theme_url_directory.'/assets/js/jquery.placeholder.js',array(),'',true);
	 	/*wp_enqueue_script('alzehor_nicescroll_js',Theme_url_directory.'/assets/js/jquery.nicescroll.min.js',array(),'',true);*/
       /* wp_enqueue_script('alzehor_twitterfeed',Theme_url_directory.'/assets/js/twitterfeed.js',array(),'',true);*/
        /*wp_enqueue_script('alzehor_jflickrfeed',Theme_url_directory.'/assets/js/jflickrfeed.min.js',array(),'',true);*/
	 	/*wp_enqueue_script('alzehor_mailChimp',Theme_url_directory.'/assets/js/mailChimp.js',array(),'',true);*/
        wp_enqueue_script('alzehor_pkgd_min',Theme_url_directory.'/assets/js/isotope.pkgd.min.js',array(),'',true);
        wp_enqueue_script('alzehor_wowslider_js',Theme_url_directory.'/assets/js/wowslider_wow.js',array(),'',true);
        wp_enqueue_script('alzehor_script_wow_slider',Theme_url_directory.'/assets/js/script_wow.js',array(),'',true);
	 	wp_enqueue_script('alzehor_script_js',Theme_url_directory.'/assets/js/script.js',array(),'',true);
	 	wp_enqueue_script('alzehor_html5_js',Theme_url_directory.'/assets/js/html5.js',array(),'',false);
        $wp_scripts->add_data('alzehor_html5_js','conditional','IE');
        /**
          * add script ajax here 
          **/
        wp_enqueue_script('woocommerce_alzohor_ajax',Theme_url_directory.'/assets/js/woocommerce_ajax.js',array(),'',true);
        wp_localize_script('woocommerce_alzohor_ajax','alzohor_action_ajax',array('ajaxurl' => admin_url('admin-ajax.php') ) );
     }

}