<?php 

add_action( 'after_setup_theme', function() {
    add_theme_support( 'woocommerce' );
} );

add_theme_support( 'wc-product-gallery-slider' );
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );

// Alter WooCommerce shop posts per page
function wpex_woo_posts_per_page( $cols ) {
    //return 2;
}
add_filter( 'loop_shop_per_page', 'wpex_woo_posts_per_page' ,10,1);

add_action( 'woocommerce_before_main_content','alzohor_output_content_wrapper_start',10);
add_action( 'woocommerce_before_shop_loop_item','alzohor_template_loop_product_link_open',10);
add_action( 'woocommerce_after_shop_loop_item','alzohor_template_loop_product_link_close',10);
add_action( 'woocommerce_before_shop_loop_item_title','alzohor_template_loop_product_thumbnail',10);
add_action( 'woocommerce_shop_loop_item_title' ,'alzohor_template_loop_product_title',10);
add_action( 'woocommerce_after_shop_loop_item_title','alzohor_after_shop_loop_item_title',10);
add_action( 'woocommerce_after_main_content' ,'alzohor_output_content_wrapper_end',10);
add_action( 'woocommerce_checkout_before_customer_details','alzohor_checkout_before_customer_details' );
add_action( 'woocommerce_checkout_after_customer_details','alzohor_checkout_after_customer_details' );
add_action( 'woocommerce_checkout_order_review','alzohor_checkout_order_review');
add_action( 'woocommerce_account_dashboard' , 'alzohor_account_dashboard',10);


function alzohor_account_dashboard(){?>
    <ul class="buttons_edite_delete">
         <li class="button_sect">
             <a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address' ) ); ?>" class="btn btn-info">تعديل عنوانك</a>
         </li>
          <li class="button_sect">
             <a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-account' ) ); ?>" class="btn btn-info">تعديل حسابك</a>
         </li>
          
    </ul>
    <hr/>
    <div class="user_avatar">
        <div class="avatar_profile">
             <?php  $url = get_avatar_url(get_current_user_id()); ?>
             <img  class="img-responsive" src="<?php echo $url; ?>" />
        </div>
        <div class="info_user_details">
             <ul >
                <?php $user_info = get_userdata( get_current_user_id() ); ?>
                 <li>
                     <span> <?php echo $user_info->user_email;  ?> </span>
                 </li>
                 <li>
                     <span> <?php echo $user_info->user_login;  ?> </span>
                 </li>
                 <li>
                     <a href="<?php echo esc_url( wc_logout_url( wc_get_page_permalink( 'myaccount' ) ) ); ?>" class="btn btn-info"> تسجيل خروج </a>
                 </li>
                 
                
             </ul>
        </div>
    </div>
<?php }
function alzohor_checkout_order_review(){
    //wc_get_template('woocommerce/checkout/review-order.php');
}

function alzohor_checkout_before_customer_details(){
    echo "<div class='form-details-input' >";
}
function alzohor_checkout_after_customer_details(){
    echo "</div>";   
    echo "<div class='order-details-input-aside' >";
         wc_get_template('woocommerce/checkout/review-order.php');
    echo "</div>";

}
function alzohor_output_content_wrapper_start(){
     if(!is_front_page() || !is_home() ){
        /*  Content Start   */
        echo "<div id='contentWrapper' class='contentWrappers'>";
    ?>
        <div class="page-title title-1">
            <div class="container">
                <h3 class="labelBanner">
                    <?php woocommerce_page_title(); ?>
                </h3>
            </div>
        </div>
    <?php } ?>
    

    <?php if(!is_front_page()){ ?>
             <div class="sectionWrapper">
                <div class="container">
                    <div class="row">
                    <div class="box success-box center hidden">Your item was added succesfully.</div>
                    <div class="clearfix"></div>
                    <?php  wc_get_template('template-parts/search-widget.php'); ?>
                    <div class="clearfix"></div>
                     <div class="grid-list">
                       <div class="row">
    <?php } ?>
<?php }

function alzohor_output_content_wrapper_end(){ ?>

<?php if(!is_front_page()): ?>
                           
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            <!-- Content End -->
<?php endif; ?>


<?php }
function alzohor_template_loop_product_title(){
        global $product;
        $link = apply_filters( 'woocommerce_loop_product_link', get_the_permalink(), $product );
        $categories = get_the_terms($product->get_id(),'product_cat');
        $all_category='';
        if(!empty($categories)){
            foreach ($categories as $category) {
                $all_category.=$category->name.' ';
            }
        } 
        ?>
        <?php if(is_front_page()): //products in Home Page show product name ?>
                <div class="name-holder">
                    <a href="<?php echo esc_url( $link );  ?>" class="project-name"> <?php echo get_the_title(); ?> </a>
                    <span class="project-options"> <?php echo (!empty($all_category)?$all_category:''); ?> </span>
                </div>
        <?php else  : //products in shop Page show product name ?>
                 <div class="item-details">
                    <h3 class="item-title">
                         <a href="<?php echo esc_url( $link );  ?>" class="project-name"> <?php echo get_the_title(); ?> </a>
                    </h3>
        <?php endif; ?>
        
            
<?php }

function alzohor_after_shop_loop_item_title(){ 
         global $product;
    ?>
    <?php if(is_front_page()) : //products in Home Page show product price and add-to-cart ?>
        <div class="item-details">
            <div class="right">
                <div class="item-price" style="background-color:transparent !important">
                     <?php  wc_get_template( 'loop/price.php' ); ?>
                </div>
            </div>
            <div class="left">
                <div class="item-cart">
                    <?php wc_get_template( 'loop/add-to-cart.php'); ?>
                </div>
            </div>
            <div class="left">
                <div class="item-cart">
                    <?php //wc_get_template( 'loop/add-to-cart.php'); ?>
                    <div class="item-cart">
                           
                        <div class="lds-roller">
                          <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
                        </div>  
                         <?php
                           /*$check_favorite = new wp_features_products_shortcode();
                           $array_favorit  = $check_favorite->handle_get_all_favorites_array(); 
                          */
                           $user = wp_get_current_user();  ?>
                          <i class="fa fa-heart-o icon_products add_to_favorite"  data-user_id="<?php echo $user->id; ?>" data-product_id="<?php echo $product->get_id(); ?>" aria-hidden="true"></i> 
                                              
                    </div>
                </div>
            </div>
        </div>
    <?php else : //products in shop Page show product price and add-to-cart ?>
            <div class="right">
               <div class="item-price" style="background-color:transparent !important">
                     <?php  wc_get_template( 'loop/price.php' ); ?>
                </div>
            </div>
            <div class="left">
                <div class="item-cart">
                     <div class="item-cart">
                         <?php wc_get_template( 'loop/add-to-cart.php'); ?>
                     </div>
                </div>
            </div>
            <div class="left">
                <div class="item-cart">
                    <?php //wc_get_template( 'loop/add-to-cart.php'); ?>
                    <div class="item-cart">
                           
                        <div class="lds-roller">
                          <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
                        </div>  
                          <?php
                         /*  $check_favorite = new wp_features_products_shortcode();
                           $array_favorit  = $check_favorite->handle_get_all_favorites_array(); 
*/
                           $user = wp_get_current_user();  ?>
                          <i class="fa fa-heart-o icon_products add_to_favorite"  data-user_id="<?php echo $user->id; ?>" data-product_id="<?php echo $product->get_id(); ?>" aria-hidden="true"></i> 
                                        
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php }



function alzohor_template_loop_product_thumbnail(){ ?>
        <?php  if(is_front_page()): //products in Home Page show product Image ?>
                        <a href="<?php echo esc_url(get_the_post_thumbnail_url()); ?>" class="fx zoom" data-gal="prettyPhoto[pp_gal]" title="">
                            <b class="fa fa-search-plus"></b>
                        </a>
                    </div>
                    <?php  echo woocommerce_get_product_thumbnail();  ?>
                </div>
        <?php   else : //products in shop Page show product image ?>
                    <div class="item-img img-holder">
                        <a href="<?php echo esc_url(get_the_permalink()); ?>" class="fx zomming_in_shop" >
                            <?php  echo woocommerce_get_product_thumbnail();  ?>
                        </a>
                    </div>
        <?php   endif; ?>
<?php }

function alzohor_template_loop_product_link_open(){
    global $product;
    $link = apply_filters( 'woocommerce_loop_product_link', get_the_permalink(), $product );
    ?>
    <?php if(is_front_page()): //products in Home Page container items ?>
                <div class="portfolio-item">
                    <div class="img-holder">
                        <div class="img-over">
                            <a href="<?php echo esc_url($link); ?>" class="fx link">
                                <b class="fa fa-link"></b>
                            </a>
    <?php  else: //products in shop Page show page container items ?>
                <div class="item-box">
    <?php  endif;  }

function alzohor_template_loop_product_link_close(){
     echo "</div>"; 
}






remove_action( 'woocommerce_before_main_content','woocommerce_output_content_wrapper_end',10);
remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb',20);
remove_action( 'woocommerce_after_main_content','woocommerce_output_content_wrapper_end',10);


add_filter( 'woocommerce_show_page_title', '__return_false' );
remove_action( 'woocommerce_before_shop_loop','woocommerce_output_all_notices',10);
remove_action( 'woocommerce_before_shop_loop','woocommerce_result_count',20);
remove_action( 'woocommerce_before_shop_loop','woocommerce_catalog_ordering',30);



remove_action( 'woocommerce_shop_loop','woocommerce_shop_loop',10);
remove_action( 'woocommerce_before_shop_loop_item','woocommerce_template_loop_product_link_open',10);
remove_action( 'woocommerce_after_shop_loop_item','woocommerce_template_loop_product_link_close',10);
remove_action( 'woocommerce_before_shop_loop_item_title','woocommerce_template_loop_product_thumbnail',10);
remove_action( 'woocommerce_after_shop_loop_item_title' ,'woocommerce_after_shop_loop_item_title');
remove_action( 'woocommerce_after_shop_loop_item_title' ,'woocommerce_template_loop_price',10);
remove_action( 'woocommerce_after_shop_loop_item' ,'woocommerce_template_loop_add_to_cart',10);
remove_action( 'woocommerce_shop_loop_item_title' ,'woocommerce_template_loop_product_title',10);
remove_action( 'woocommerce_after_shop_loop_item_title','woocommerce_template_loop_price',10);
//remove_action( 'woocommerce_after_shop_loop','woocommerce_pagination',10);
remove_action( 'woocommerce_after_shop_loop_item_title' ,'woocommerce_template_loop_rating',5);

remove_action( 'woocommerce_after_main_content' ,'woocommerce_output_content_wrapper_end',10);
remove_action( 'woocommerce_checkout_order_review','woocommerce_order_review',10);
