<?php 
class wp_custom_action_theme{
     
     function __construct(){
         add_action('show_dropdown_taxonomy_product',array($this,'show_dropdown_taxonomy_product'));
         add_action('show_dropdown_prices_search',array($this,'show_dropdown_prices_search'));
         add_action('show_dropdown_taste_search',array($this,'show_dropdown_taste_search'));
     }

     function content_filter_search(){
     	ob_start();
        ob_get_clean();
     }

     function show_dropdown_taxonomy_product(){ 
        
        $select_alzohor .='<select name="product_Category" class="dropdown">';
        $select_alzohor .='<option value="" "'.( ( empty( $_GET['Category'] ) )?'selected':'').'">اختر</option>';
        $all_taxonomy=get_terms('product_cat');

        foreach ($all_taxonomy as $items) {
            $select_alzohor .='<option value="'.$items->name.'" '.( ( !empty( $_GET['Category'] ) && ($_GET['Category']==$items->name) )?'selected':'').' >'.$items->name.'</option>';
        }

        $select_alzohor .='</select>'; 
        //var_dump($all_taxonomy);
        echo $select_alzohor;
    
     }

     function show_dropdown_prices_search($class_name){
        $select_price_alzohor   .='<select name="'.$class_name.'" class="'.$class_name.'">';
        $select_price_alzohor   .='<option value="" '.( ( empty( $_GET[$class_name] ) )?'selected':'').'>اختر</option>
                                 <option value="1" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==1) )?'selected':'').'>1</option>
                                 <option value="100" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==100) )?'selected':'').'  >100</option>
                                 <option value="200" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==200) )?'selected':'').'  >200</option>
                                 <option value="300" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==300) )?'selected':'').' >300</option>
                                 <option value="400" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==400) )?'selected':'').'>400</option>
                                 <option value="500" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==500) )?'selected':'').'>500</option>
                                 <option value="600" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==600) )?'selected':'').'>600</option>
                                 <option value="700" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==700) )?'selected':'').'>700</option>
                                 <option value="800" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==800) )?'selected':'').'>800</option>
                                 <option value="900" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==900) )?'selected':'').'>900</option>
                                 <option value="1000" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==1000) )?'selected':'').'>1000</option>
                                 <option value="2000" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==2000) )?'selected':'').'>2000</option>
                                 <option value="3000" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==3000) )?'selected':'').'>3000</option>
                                 <option value="4000" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==4000) )?'selected':'').'>4000</option>
                                 <option value="5000" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==5000) )?'selected':'').'>5000</option>
                                 <option value="6000" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==6000) )?'selected':'').'>6000</option>
                                 <option value="7000" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==7000) )?'selected':'').'>7000</option>
                                 <option value="8000" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==8000) )?'selected':'').'>8000</option>
                                 <option value="9000" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==9000) )?'selected':'').'>9000</option>
                                 <option value="10000" '.( ( !empty( $_GET[$class_name] ) && ($_GET[$class_name]==10000) )?'selected':'').'>10000</option>';
        $select_price_alzohor   .='</select>';
        
        echo $select_price_alzohor;
     }


    function show_dropdown_taste_search(){
            echo '<li><h3><a href="#"> نكهة </a></h3>';
            echo '<div class="accordion-panel active"><div class="control-group">';
            $all_products_attr=get_terms('pa_taste');
            foreach ($all_products_attr as $attr_product){ ?>
                    <label class="contcheckbox"> <?php echo $attr_product->name; ?>
                        <input name="checkbox_input[]" class="taste_checkbox" type="checkbox" value="<?php echo $attr_product->name; ?>" <?php echo ( ( !empty( $_GET['taste'] ) && (in_array($attr_product->name, explode('-',$_GET['taste']) ) )  )?'checked':''); ?> >
                        <span class="checkmark"></span>
                    </label>               
           
            <?php 
            }
            echo '</div></div></li>';
      

    }


}