<?php 
class wp_api_commerce_products_category extends wp_api_commerce  {
	function __construct(){
		/**
		 *  here api link is http://localhost/wordpress/api?key_auth=123456&part=all-products
		 *  consist of part = all-products
  		 *  search_text = 
  		 *  count
  		 *  type = most_sale - height_rate  
  		 *  vendor_id 
  		 *  here api link is http://localhost/wordpress/api?key_auth=123456&part=all-categories
  		 *  search_text =  
		**/
       $this->get_authentaction_request();
       if($this->key_auth_status===true):
             if(!empty($_GET['part'])):
                  if($_GET['part']=='all-products'):
                  	  $this->get_all_products();
                  elseif($_GET['part']=='all-categories'):
                  	  $this->get_all_categories();
                  endif;
             endif;
        endif;
	}

	function get_all_products(){	
		/**
		*  here handle or setting attr for post api
		**/
		$all_product_details=array();
		$all_images=array();
        $number_posts=(!empty( $_GET['count'] )?$_GET['count']:'-1');
		if(!empty($_GET['type'])){
			$args=array(
		                'post_type'=>'product',
		                'posts_per_page'=>$number_posts,
		                'order'=>'asc',
		         );

            $all_types=explode('-', $_GET['type']);
			if(in_array('most_sale', $all_types)){
	            $most_sale = array('meta_key' => 'total_sales',
                                   'orderby' => 'meta_value_num');

	            array_merge($args,$most_sale);
		    }
		    if(in_array('hight_review', $all_types)){

		    	$most_review = array('orderby' => 'average_rating');

		    	array_merge($args,$most_review);
	             
		    }
		}
		else
		{

	        $args=array(
	                'post_type'=>'product',
	                'posts_per_page'=>$number_posts,
	                'order'=>'asc',
	        	);
		}

		if(!empty($_GET['vendor_id']))
		{
			$vendor_meta_query=array(
				    'tax_query' =>array()
				);

			array_merge($args,$vendor_meta_query);
			$args['tax_query']=array(
				                   array(
			                         'taxonomy' =>'yith_shop_vendor',
			                         'field'    =>'term_id',
			                         'terms'    =>$_GET['vendor_id']
							    	)
				                );
		}

		if(!empty( $_GET['search_text'] ) ){
              $search_auto_complete=$_GET['search_text'];
		}

        $all_products=new WP_QUERY($args);
        if($all_products->have_posts()):
        	while($all_products->have_posts()): $all_products->the_post();
                  $product=new WC_Product( get_the_ID() );
                  /**
				    * here setttings product name
				    **/
                   $product_name= $product->get_name();
                   /**
                     * here search by title of product %s
                     **/
                   if( (!empty($search_auto_complete) ) && ( $search_auto_complete!=$product_name ) ){
                      continue;
                   }
                   /**
                    * here settings product images
                    **/
                   $attachment_ids = $product->get_gallery_image_ids();
                   if ( $attachment_ids && $product->get_image_id() ) {
						foreach ( $attachment_ids as $attachment_id ) {
							$all_images[]=wp_get_attachment_image_src($attachment_id,'full')[0];
						}
					}
					if( !empty( get_the_post_thumbnail_url() ) ){
					     array_push($all_images, get_the_post_thumbnail_url() );
					}


					/**
					  * get reviews and rates 
					  **/
					  $args = array ('post_type' => 'product','post_id'=>get_the_ID() );
		              $comments = get_comments( $args );
		              foreach ($comments as $comment) {
		              	    $all_comments_review[]=array('commenter_name'=>$comment->comment_author,
		              	    	                         'review_value'  =>intval( get_comment_meta( $comment->comment_ID, 'rating', true ) ),
		              	    	                         'review_date'   =>$comment->comment_date,
		              	    	                         'review_body'   =>$comment->comment_content
		              	    	                   );
		              	
		              }
					 
					


                     /**
					 * here get vendor name and vendor id
					 **/
                    $all_data_vendors = get_the_terms(get_the_ID() ,'yith_shop_vendor');
                    if(!empty($all_data_vendors)){
	                    foreach($all_data_vendors as $vendor_details) {
	                    	$vendor_array=array('name'=>$vendor_details->name,'id'=>$vendor_details->term_id);
	                    }
	                }

                    if(empty($all_data_vendors)){
                    	$vendor_array='';
                    }



                  $all_product_details[]=array(
                  	                           'product_details'=>array(
                  	                                'images'=> $all_images,
                  	                                'title'  => $product_name,
                  	                                'id'    => get_the_ID(),
                  	                                'rate'  => array( 'review_allow'  =>$product->get_reviews_allowed(),
                  	                                	              'rate_average'  =>$product->get_average_rating(),
                  	                                	              'rate_count'    =>$product->get_rating_counts() ,
                  	                                	              'review_count'  =>$product->get_review_count()),
                  	                                'prices' => array( 'price'        =>$product->get_price(),
                  	                                	               'regular_price'=>$product->get_regular_price()),
                  	                                'offers' => $product->get_sale_price(),
                  	                                'stocks' => array('stock_allow'    =>$product->get_manage_stock(),
                  	                                	              'stock_status'   =>$product->get_stock_quantity(),
                  	                                	              'stocks_quantity'=>$product->get_stock_status()),
                  	                                'descriptions' => array('description'=>$product->get_description(),
                                                                            'short_description'=>$product->get_short_description()),
                  	                                'catogery'     => strip_tags($product->get_categories()),
                  	                                'attributes'   => $product->get_attributes(),
                  	                                'reviews'      => $all_comments_review,
                  	                                'vendors'      =>$vendor_array,

                  	                            )
                  	                     );

                  unset($all_images);
                  unset($all_comments_review);
                  unset($vendor_array);

        	endwhile;
        endif;
        echo json_encode(array($all_product_details));
		exit();


	}



	function get_all_categories(){
	    $all_categories=get_terms(array('taxonomy'=>'product_cat','hide_empty'=>false));
        $all_categories_json=array();
        if(!empty( $_GET['search_text'] ) ){
              $search_auto_complete_category=$_GET['search_text'];
		}
		foreach ($all_categories as $category):
			$thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true ); 
		    // get the image URL
		    $image = wp_get_attachment_url( $thumbnail_id );

		           /**
                     * here search by title of category %s
                     **/
                   if( (!empty($search_auto_complete_category) ) && ( $search_auto_complete_category!=$category->name ) ){
                      continue;
                   }

             $all_categories_json[]=array( 'name'   =>$category->name,
                                       'id'    =>$category->term_id,
                                       'image' =>$image
             	);
		endforeach;
        echo json_encode(array('all-categories'=>$all_categories_json));
        exit();

	}

	function get_most_sale_product(){

	}


	function status_wait_complete_request(){	
		/**
		*  here start  for get api
		**/

		 echo json_encode(array("status" =>'request not found in our requests'));
		 exit();
	}


	


}