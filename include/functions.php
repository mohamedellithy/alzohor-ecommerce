<?php 
ob_start();
require_once Theme_url.'/include/class-wp-bootstrap-navwalker.php';
require_once Theme_url.'/include/class-wp-install-styles-scripts-files-theme.php';
require_once Theme_url.'/include/class-wp-install-menus-theme.php';
require_once Theme_url.'/include/class-wp-widget-search-filter.php';
require_once Theme_url.'/include/class-wp-widget-high-sale.php';
require_once Theme_url.'/include/class-wp-custom-action-theme.php';
require_once Theme_url.'/include/woocommerce-products.php';
require_once Theme_url.'/include/woocommerce-cart.php';
require_once Theme_url.'/include/class-wp-filter-ajax-search.php';
require_once Theme_url.'/include/woocommerce-single-product.php';
require_once Theme_url.'/include/class-wp-show-all-vendors.php';
require_once Theme_url.'/include/class-wp-api-commerce.php';
require_once Theme_url.'/include/class-wp-api-commerce-products-category.php';
require_once Theme_url.'/include/class-wp-api-commerce-users.php';
require_once Theme_url.'/include/class-wp-api-commerce-vendors.php';
require_once Theme_url.'/include/class-wp-features-products-shortcode.php';


/**
 * add theme options and settings
 * all options added by mohamed ellithy
 * @package 
 **/

/* add theme styles and scripts files  */
new wp_install_styles_scripts_file_theme();

/*add shortcodes */
new wp_custom_action_theme();

/* add theme widgets*/
//new wp_widget_search_filter();
function add_advanced_search_sidebar(){
	register_widget('wp_widget_search_filter');
}
add_action( 'widgets_init', 'add_advanced_search_sidebar' );


//new wp_widget_hight_sale();
function add_high_sale_sidebar(){
	register_widget('wp_widget_high_sale');
}
add_action( 'widgets_init', 'add_high_sale_sidebar' );



/**
 * Add a sidebar.
 */
function wpdocs_theme_slug_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Alzohor Search Filter Sidebar', 'textdomain' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Widgets in this area will be shown on all options in woocommerce theme alzohor.', 'textdomain' ),
        'before_widget' => '<aside class="cell-3 left-shop"> <li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li></aside>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'wpdocs_theme_slug_widgets_init' );



/* add theme menu options   */
new wp_install_menus_theme();


function show_header_menu(){
        wp_nav_menu(array( 
	        'theme_location'=>'header-alzohor-menu',
	        'menu_class'    =>'def-effect',
	        'container'     =>false,
	        'link_before'    =>'<span>',
	        'depth'         =>2,
	        'walker'        =>'',
	  	));
}


function show_footer_menu(){
     wp_nav_menu(array(
            'theme_location'=>'footer-alzohor-menu',
            'menu_class'    =>'footer-menu',
            'container'     =>false,
            'depth'         =>2,
            'walker'        =>'',
        ));
}



/*add_action('init','my_custom_page_word');
function my_custom_page_word() {
	global $wp_rewrite;
	$wp_rewrite->pagination_base = "leht";
	unset($wp_rewrite->extra_rules_top["tooted-teenused/page/([0-9]{1,})/?$"]);
	$wp_rewrite->extra_rules_top['tooted-teenused/leht/([0-9]{1,})/?$'] = 'index.php?post_type=product&paged=$matches[1]';
}*/


add_filter('use_block_editor_for_post', '__return_false', 10);


new wp_filter_ajax_search();
new wp_show_all_vendors();




add_action('wp_ajax_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
        
function woocommerce_ajax_add_to_cart() {

            $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
            $quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
            $variation_id = absint($_POST['variation_id']);
            $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
            $product_status = get_post_status($product_id);

            if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity,$variation_id) && 'publish' === $product_status) {

                do_action('woocommerce_ajax_added_to_cart', $product_id);

                if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
                    wc_add_to_cart_message(array($product_id => $quantity), true);
                }

                WC_AJAX :: get_refreshed_fragments();
               /* $product = new WC_Product( get_the_ID() );
                $link_url_image_in_alert = get_the_post_thumbnails_url( get_the_ID() );*/
                /*$template_alert_message='<div class="alert-add-to-cart">
                                            <div class="close_alert_cart">
                                                <i class="fa fa-remove"></i>
                                            </div>
                                            <div class="img-alert-cart">
                                                <img src="'.$link_url_image_in_alert.'" >
                                            </div>
                                            <div class="item-cart-details-alert-cart">
                                                <div class="title-alert-cart">
                                                    <h4>'.$product->get_name().'</h4>
                                                </div>
                                                <div class="amount-price-alert-cart">
                                                    <label>'.$quantity.' * '.wc_get_template( 'loop/price.php' ).'</label>
                                                </div>
                                                <div class="total-price-alert-cart">
                                                    <label>  120 EGP</label>
                                                </div>
                                            </div>
                                        </div>';*/
               /* $template_alert_message = "<div class='alert-add-to-cart'></div>";
                //echo $template_alert_message;
                echo $template_alert_message;*/

            } else {

                $data = array(
                    'error' => true,
                    'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id));

                //echo 'null';
                //echo wp_send_json($data);
            }

            wp_die();
        }





add_action('wp_ajax_woocommerce_ajax_remove_item_from_cart', 'woocommerce_ajax_remove_item_from_cart');
add_action('wp_ajax_nopriv_woocommerce_ajax_remove_item_from_cart', 'woocommerce_ajax_remove_item_from_cart');

function woocommerce_ajax_remove_item_from_cart(){
	global $woocommerce;
	if(!empty($_POST)){
		 $woocommerce->cart->remove_cart_item($_POST['cart_item_key']);
	}
	wp_die();
}





add_action('wp_ajax_woocommerce_ajax_add_to_favorite', 'woocommerce_ajax_add_to_favorite');
add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_favorite', 'woocommerce_ajax_add_to_favorite');

function woocommerce_ajax_add_to_favorite(){
   /* global $woocommerce;
    if(!empty($_POST)){
         $woocommerce->cart->remove_cart_item($_POST['cart_item_key']);
    }*/
    global $wpdb;
    $user_id = $_POST['user_id'];
    $product_id = $_POST['product_id'];
    if(!empty($user_id)){
        $table_name=$wpdb->prefix.'_favorites';
            $wpdb->insert($table_name, array(
               "user_id" => $user_id,
               "product_id" => $product_id,
            ));
        $link_redirect='no';
        echo $link_redirect;
        
    }
    else
    {
        $link_redirect = get_page_link(get_option( 'woocommerce_myaccount_page_id' ));
        echo $link_redirect;

    }

    wp_die();
}




/**
  *  extract all api for * products & category
  *  extract all api for * users
  *  extract all api for * vendors
  **/
new wp_api_commerce();
new wp_api_commerce_products_category();
new wp_api_commerce_users();
new wp_api_commerce_vendors();



/**
  * show all features procudts
  **/
new wp_features_products_shortcode();







        

ob_end_flush();