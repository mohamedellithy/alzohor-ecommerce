<?php 

class wp_show_all_vendors extends YITH_Vendor {

	public $shops_search_ids;
	public $link=null;
	public $all_attrs_link;
	function __construct(){

        add_shortcode('show_all_vendors',array($this,'show_all_vendors') );
        add_action('show_all_shops',array($this,'panel_for_shop') );
        add_action('show_all_shops_category_search',array($this,'show_shops_in_sidebar_search') );
        $this->filter_search_action_shops();
	}

	function filter_search_action_shops(){
    	$this->link='?type=search_filter_shops';
		if(isset($_POST['search_shop'])){
             $shops_id=$_POST['shop_name'];	
             $this->link.='&shops_id='.implode('-', $shops_id);	
             wp_redirect($this->link);
             exit;
		}


    }

	

	function show_all_vendors(){
		ob_start();
		
		get_template_part('template-parts/content','show_shops');
		ob_end_flush();
	}

	function args_loop_shops_start($type){

		$this->shops_search_ids=( (!empty($_GET['shops_id']) )?explode('-', $_GET['shops_id']):null);
		
		 $args=[
		        'taxonomy'=>'yith_shop_vendor',
		        'hide_empty'=>false,
		        'term_taxonomy_id'   =>( (!empty($type) )?( (!empty($this->shops_search_ids) )?$this->shops_search_ids:null):null)
		    ];
		    $all_shops = get_terms($args);
		    return $all_shops;

		   
	}
	
	function panel_for_shop(){
		            $all_shops = $this->args_loop_shops_start($type='content');
                    foreach ($all_shops as $shop) { 
		             ?>

				    <div class="cell-4 fx shop-item" data-animate="fadeInUp">
			            <div class="item-box item-box2">

			                <div class="item-img">
			                    <a href="">

			                        <?php
                                        if ( ! class_exists( 'YITH_Vendor' ) ) {
			                                $this->id = $shop->term_id;
			                                $user_id = $this->get_owner();
			                               
			                            }

			                            if( !empty($user_id)){
			                            	 $image_url=esc_url(get_avatar_url($user_id));
			                            }
			                            else
			                            {
			                            	$image_url=esc_url(get_template_directory_uri().'/assets/images/morrisons_paperwrappedflowers_03-b452-e1535630002192.jpg');
			                            }
			                        ?>
			                        <img alt="" src="<?php echo $image_url; ?> ">
			                    </a>
			                </div>
			                <div class="item-details">
			                    <h3 class="item-title">
			                        <a href="<?php echo esc_url(get_term_link($shop->term_id)); ?>"><?php echo $shop->name; ?></a>
			                    </h3>


			                    <div class="ReadMoreA">
			                        <a href="<?php echo esc_url(get_term_link($shop->term_id)); ?>"> المزيد  </a>

			                    </div>
			                </div>
			            </div>
			        </div>
		    	
		    
     <?php 

       }

		   

	}

	public function show_shops_in_sidebar_search(){ ?>
    <form method="POST">
		<ul id="accordion" class="accordion">
	        <li>
	            <h3>
	                <a href="#"> اختر اسم المحل </a>
	            </h3>
	            <div class="accordion-panel active">
	                <div class="control-group">

					    <?php 
				            $all_shops = $this->args_loop_shops_start($type);
				            foreach ($all_shops as $shop) {
						?>
	                        <label class="contcheckbox">  <?php echo $shop->name; ?>
	                            <input name="shop_name[]" type="checkbox" value="<?php echo $shop->term_id; ?>" <?php echo ( ( !empty( $_GET['shops_id'] ) && (in_array($shop->term_id, explode('-',$_GET['shops_id']) ) )  )?'checked':''); ?> >
	                            <span class="checkmark"></span>
	                        </label>
			             
			             <?php  } ?>

			     
	                </div>
	            </div>
	        </li>
	    </ul>
	    <input name="search_shop" type="submit" class="btn btn-small main-bg" value="بحث " style="width: 100%;" />
	</form>

 
    <?php 		
	}


}