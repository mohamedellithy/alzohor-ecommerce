<?php 
class wp_api_commerce_vendors extends wp_api_commerce {
	function __construct(){
		/**
		 * http://akk.cke.mybluehost.me/api?key_auth=123456&part=all-vendors
		 * part = 
		 **/
		 $this->get_authentaction_request();
		if($this->key_auth_status===true):
             if(!empty($_GET['part'])):
                  if($_GET['part']=='all-vendors'):
                  	if(class_exists('YITH_Vendor')):
                  	     $this->get_all_vendors();
                  	endif;
                  endif;
             endif;
        endif;
	}

	function get_all_vendors(){
		$number_posts=( ( !empty($_GET['count']) )?$_GET['count']:false);
        $args=[
		        'taxonomy'  =>'yith_shop_vendor',
		        'hide_empty'=>false,
		        'number'    =>$number_posts,          
		        /*'term_taxonomy_id'   =>( (!empty($type) )?( (!empty($this->shops_search_ids) )?$this->shops_search_ids:null):null)*/
		    ];
		$all_vendors = get_terms($args);
        if(!empty( $_GET['search_text'] ) ){
              $search_auto_complete_category=$_GET['search_text'];
		}
		$all_vendors_details=array();
        if(!empty($all_vendors)){
			foreach ($all_vendors as $vendor) {

				$vendor_owner=get_userdata($this->get_owner() );

				   /**
                     * here search by title of category %s
                     **/
                   if( (!empty($search_auto_complete_category) ) && ( $search_auto_complete_category!=$vendor->name ) ){
                      continue;
                   }

	             $all_vendors_details[]= array('vendor_details'   => array(
	             	                                      'name'  =>$vendor->name,
	             	                                      'id'    =>$vendor->term_id,
	             	                                      'owner' =>$vendor_owner->user_login,
	             	                                      'image' =>esc_url( get_avatar_url( $this->get_owner() ) )
	             	                                )
	                                    );
	     		
			}

	        echo json_encode(array("vendor_details" =>$all_vendors_details));
	    } else {
	        echo json_encode(array("vendor_details" =>['empty']));	
	    }
        exit();
	}
}