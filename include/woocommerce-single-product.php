<?php 

add_action( 'woocommerce_before_single_product_summary' ,'alzohor_show_product_images',20);
add_action( 'woocommerce_single_product_summary' ,'alzohor_template_single_title',5);
add_action( 'woocommerce_single_product_summary' ,'alzohor_template_single_price',10);
add_action( 'woocommerce_single_product_summary' ,'alzohor_template_single_rating',10);
add_action( 'woocommerce_single_product_summary' ,'alzohor_template_single_add_to_cart',20);
add_action( 'woocommerce_after_single_product_summary' ,'alzohor_output_product_data_tabs',10);
//add_action( 'woocommerce_after_single_product_summary' ,'alzohor_output_related_products',20);
add_action( 'woocommerce_review_before', 'alzohor_review_display_gravatar',10,1);
add_action( 'woocommerce_review_after_comment_text','alzohor_review_after_comment_text' ,10,1); 


function alzohor_show_product_images(){ ?>
	<div class="cell-9">
		<div class="cell-4">
			<div class="product-img">
				<img alt="" id="img_01" src="<?php echo esc_url(get_the_post_thumbnail_url()); ?>" />
				<div class="thumbs">
					<ul id="gal1">
	                   <?php wc_get_template( 'single-product/product-thumbnails.php' ); ?>
					</ul>
				</div>
			</div>
		</div>

<?php }
function alzohor_template_single_title(){ ?>
      <div class="cell-8">
      	<div class="product-specs price-block list-item">
      		<div class="price-box">
      			<?php wc_get_template( 'single-product/title.php' ); ?>
      	   </div>
      	   


<?php }
function alzohor_template_single_price(){ ?>     
        <div class="price-box">
      	   	   <span class="product-price"><?php wc_get_template( 'single-product/price.php' );  ?></span>
      	</div>
<?php }

/*function alzohor_output_related_products(){ ?>
        <!-- <div class=" label-section2 logo_in_pages related_products_hr">
		        <h2><span>المنتجات المرتبطة</span></h2>
		</div>
		<div class="container">
			
		</div> -->

<?php }*/
function alzohor_template_single_rating(){ ?> 
        <div>
			<span class="item-rating">
				<!-- <span class="fa fa-star"></span>
				<span class="fa fa-star"></span>
				<span class="fa fa-star"></span>
				<span class="fa fa-star-o"></span>
				<span class="fa fa-star-o"></span> -->
				<?php   wc_get_template( 'single-product/rating.php' ); ?>
			</span>

		</div>
	</div>

<?php }
function alzohor_template_single_add_to_cart(){ ?>
        
		<div class="list-item product-block item-add">
             <?php  wc_get_template( 'single-product/add-to-cart/simple.php' ); ?>
        </div>
        <div class="clearfix"></div>
		<div class="padd-top-20"></div>
		<div class="clearfix padd-bottom-20"></div>
<?php 
 }

 function alzohor_output_product_data_tabs(){ global $product; ?>
        <div class="cell-12">
			<div class="row">
				<div id="tabs" class="tabs">
					<ul>
						<li class="skew-25 active"><a href="#" class="skew25"> وصف </a></li>
						<li class="skew-25"><a href="#" class="skew25"> تفاصيل المنتج </a></li>
						<li class="skew-25"><a href="#" class="skew25"> أراء الجمهور</a></li>
			 
					</ul>
					<div class="tabs-pane">
						<div class="tab-panel active">
							<p>  
                               <?php wc_get_template( 'single-product/tabs/description.php' ); ?>

                            </p>
						 
						</div>
						<div class="tab-panel">
							<h4><strong> جدول عن المنتج وتفاصيلة </strong> </h4>
							 
							<table>
								<tr>
									<th colspan="2" class="left-text">General</th>
								</tr>
								<tr>
									<td class="width150">اسم المنتج: </td>
									<td><?php echo $product->name; ?></td>
								</tr>
								<tr>
									<td class="width150">الوزن: </td>
									<td><?php echo $product->weight; ?></td>
								</tr>
								<tr>
									<td class="width150">الطول: </td>
									<td><?php echo $product->length; ?></td>
								</tr>
								

								
							</table>
							<?php wc_get_template( 'single-product/tabs/additional-information.php' ); ?>
							<p>
								يتم استخدام نص لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ  للأحرف الموجودة في اللغة العربية بدلا من أو  عوضاً عن استخدام “هنا يوجد محتوى نصي، هنا يوجد محتوى نصي”  و تكرارها كثيرا في بعض المواقع الالكترونية الحديثة فيستخدم نص لوريم ايبسوم فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء.
							</p>
						</div>
						<div class="tab-panel">
							 <div class="reviews">
								 <div class="hidden">
									 <p class="hint hintMargin">This item has no reviews yet.</p>
									 <p>Be the first one and review it now:<br><br><a class="btn main-bg btn-medium">Write a Review</a></p>
								 </div>
							 
								<div class="clearfix"></div>
								<div class="comments">
									<h3 class="block-head"> أراء الجمهور </h3>
									
									<?php wc_get_template( 'single-product/tabs/tabs.php' );  ?>


								   <!--  <ul class="comment-list">
								        <li>
								            <article class="comment">
								                <div class="comment-content">
								                    <h5 class="comment-author skew-25">
								                        <span class="author-name skew25">احمد محمد</span>
								                        <span class="item-rating">
															<span class="fa fa-star skew25"></span><span class="fa fa-star skew25"></span><span class="fa fa-star skew25"></span><span class="fa fa-star-o skew25"></span><span class="fa fa-star-o skew25"></span>
														</span>
								                        <span class="comment-date skew25">Jan 15, 2014</span>
								                    </h5>
								                    <p>نص  عن تجربتة من خلال المنتج</p>
								                </div>
								            </article>
								        </li>
								        <li>
								            <article class="comment">
								                <div class="comment-content">
								                    <h5 class="comment-author skew-25">
								                        <span class="author-name skew25">احمد محمد</span>
								                        <span class="item-rating">
															<span class="fa fa-star skew25"></span><span class="fa fa-star skew25"></span><span class="fa fa-star skew25"></span><span class="fa fa-star-o skew25"></span><span class="fa fa-star-o skew25"></span>
														</span>
								                        <span class="comment-date skew25">Jan 15, 2014</span>
								                    </h5>
								                    <p>نص  عن تجربتة من خلال المنتج</p>
								                </div>
								            </article>
								        </li>
								        <li>
								            <article class="comment">
								                <div class="comment-content">
								                    <h5 class="comment-author skew-25">
								                        <span class="author-name skew25">احمد محمد</span>
								                        <span class="item-rating">
															<span class="fa fa-star skew25"></span><span class="fa fa-star skew25"></span><span class="fa fa-star skew25"></span><span class="fa fa-star-o skew25"></span><span class="fa fa-star-o skew25"></span>
														</span>
								                        <span class="comment-date skew25">Jan 15, 2014</span>
								                    </h5>
								                    <p>نص  عن تجربتة من خلال المنتج</p>
								                </div>
								            </article>
								        </li>
								    </ul> -->
								</div>
								<div class="clearfix"></div>
								
							</div>
						</div>
						
					</div>
				</div>
          
<?php  

      // wc_get_template( 'single-product/rating.php' );
}
function alzohor_review_display_gravatar($comment){
   
}

function alzohor_review_after_comment_text($comment){?>
      </div>
	</article>
<?php }

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

unset( $tabs['description'] ); // Remove the description tab
unset( $tabs['additional_information'] ); // Remove the additional information tab

return $tabs;

}


remove_action( 'woocommerce_before_single_product_summary' ,'woocommerce_show_product_images',20);
remove_action( 'woocommerce_single_product_summary' ,'woocommerce_template_single_title',5);
remove_action( 'woocommerce_single_product_summary' ,'woocommerce_template_single_price',10);
remove_action( 'woocommerce_single_product_summary' ,'woocommerce_template_single_rating',10);
remove_action( 'woocommerce_single_product_summary' ,'woocommerce_template_single_add_to_cart',30);
remove_action( 'woocommerce_single_product_summary' ,'woocommerce_template_single_meta',40);
remove_action( 'woocommerce_single_product_summary' ,'woocommerce_template_single_excerpt',20);
//remove_action( 'woocommerce_after_single_product_summary' ,'woocommerce_output_related_products',20);
remove_action( 'woocommerce_after_single_product_summary' ,'woocommerce_output_product_data_tabs',10);
remove_action( 'woocommerce_review_before', 'woocommerce_review_display_gravatar',10,1);
remove_action( 'woocommerce_review_before_comment_meta', 'woocommerce_review_display_rating',10,1 );