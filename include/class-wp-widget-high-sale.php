<?php 

class wp_widget_high_sale extends WP_Widget {
    
    function __construct(){
      	parent::__construct(
	      		// widget ID
				'hight_sale_widget',
				// widget name
				__('Alzohor Hight Sale ', 'alzohor'),
				// widget description
				array ( 'description' => __( 'Alzohor woocommerce Hight Sale', 'hstngr_widget_domain' ), )
      		);
    }

    public function widget( $args, $instance ) {
		$title  = apply_filters( 'widget_title', $instance['title'] );
		$number_of_products  = apply_filters( 'widget_number_of_products', $instance['number_of_products'] );
		?>
                <div class="widget r-posts-w sale-widget fx" data-animate="fadeInRight">
                    <h3 class="widget-head"> <?php echo $title;?> </h3>
                    <div class="widget-content">
                        <ul>
                            <?php  
                             /**
                              * show hight sale in from product
                              **/
                              $args=[ 
                                      'post_type'=>'product',
                                      'posts_per_page'=>3,
                                      'meta_key' => 'total_sales',
                                      'orderby' => 'meta_value_num',
                                      
                              ];

                              $hight_sale_product=new WP_QUERY($args);
                                if($hight_sale_product->have_posts()){
                                    while($hight_sale_product->have_posts()) { $hight_sale_product->the_post();
                                        global $product;
                                        $rating_count = (!empty($product->get_rating_count())?$product->get_rating_count():0);
                                        $review_count = (!empty($product->get_review_count())?$product->get_review_count():0);
                                        $average      = (!empty($product->get_average_rating())?$product->get_average_rating():0);
                                             
                                ?>

                                        <li>
                                            <div class="post-img">
                                               <?php the_post_thumbnail(); ?>
                                            </div>
                                            <div class="widget-post-info">
                                                <h4>
                                                    <a href="#"> <?php the_title(); ?> </a>
                                                </h4>
                                                <div class="meta">
                                                    <div class="item-rating">
                                                        <span class="item-rating">
                                                            <?php   for($i=1; $i <=$average  ; $i++) { 
                                                                         echo '<span class="fa fa-star"></span>';
                                                                    }
                                                                    for($j=0; $j<5-$average; $j++) { 
                                                                         echo '<span class="fa fa-star-o"></span>';
                                                                    }
                                                                    echo ' ( '.$review_count.' /'.$average.') ';
                                                             ?>
                                                        </span>
                                                    </div>
                                                </div>

                                            </div>
                                         </li>

                                <?php  
                                   }
                                }
                            ?>
                        </ul>
                    </div>
                </div>

            

		<?php 


    }

    public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ){
		     $title = $instance[ 'title' ];
		}
		else
		{
		     $title = __( 'Default Title', 'hstngr_widget_domain' );
		}

		

		if ( isset( $instance[ 'number_of_products' ] ) ){
		     $number_of_products = $instance[ 'number_of_products' ];
		}
		else
		{
		     $number_of_products = __('3', 'hstngr_widget_domain' );
		}

		if ( isset( $instance[ 'text_hight_sale' ] ) ){
		     $text_hight_sale = $instance[ 'text_hight_sale' ];
		}
		else
		{
		     $text_hight_sale = __('[content-hight-sale]', 'hstngr_widget_domain' );
		}



		?>
		<p>
		    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>

		

		<p>
		    <label for="<?php echo $this->get_field_id( 'number_of_products' ); ?>"><?php _e( 'Number Of Products:' ); ?></label>
		    <input class="widefat" id="<?php echo $this->get_field_id( 'number_of_products' ); ?>" name="<?php echo $this->get_field_name( 'number_of_products' ); ?>" type="number" value="<?php echo esc_attr( $number_of_products ); ?>" />
		</p>

		<p>
		    <label for="<?php echo $this->get_field_id( 'text_hight_sale' ); ?>"><?php _e( 'content:' ); ?></label>
		    <input class="widefat" id="<?php echo $this->get_field_id( 'text_hight_sale' ); ?>" name="<?php echo $this->get_field_name( 'text_hight_sale' ); ?>" type="text" value="<?php echo esc_attr( $text_hight_sale ); ?>" readonly/>
		</p>

		

		<?php
    }


    public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['number_of_products'] = ( ! empty( $new_instance['number_of_products'] ) ) ? strip_tags( $new_instance['number_of_products'] ) : '';
		$instance['text_hight_sale'] = ( ! empty( $new_instance['text_hight_sale'] ) ) ? strip_tags( $new_instance['text_hight_sale'] ) : '';
		return $instance;
	}




}