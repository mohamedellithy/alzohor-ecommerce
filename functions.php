<?php


 /**
 * alzehor functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage alzehor
 * @since 1.0
 */
 ob_start();
define('Theme_url',get_template_directory() );
define('Theme_url_directory',get_template_directory_uri() );


require_once Theme_url.'/include/functions.php';
add_theme_support( 'widgets' );
add_theme_support( 'customize-selective-refresh-widgets' );


function be_exclude_category_from_blog( $query ) {
	
	if( $query->is_main_query() && ! is_admin() && $query->is_home() ) {
		$query->set( 'cat', '-4' );
	}
}
add_action( 'pre_get_posts', 'be_exclude_category_from_blog' );



add_filter( 'show_admin_bar', '__return_false' );

/*$new_short_description="يتم استخدام نص لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ  للأحرف الموجودة في اللغة العربية بدلا من أو  عوضاً عن استخدام “هنا يوجد محتوى نصي، هنا يوجد محتوى نصي”  و تكرارها كثيرا في بعض المواقع الالكترونية الحديثة فيستخدم نص لوريم ايبسوم فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. يتم استخدام نص لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ  للأحرف الموجودة في اللغة العربية بدلا من أو  عوضاً عن استخدام “هنا يوجد محتوى نصي، هنا يوجد محتوى نصي”  و تكرارها كثيرا في بعض المواقع الالكترونية الحديثة فيستخدم نص لوريم ايبسوم فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. يتم استخدام نص لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ  للأحرف الموجودة في اللغة العربية بدلا من أو  عوضاً عن استخدام “هنا يوجد محتوى نصي، هنا يوجد محتوى نصي”  و تكرارها كثيرا في بعض المواقع الالكترونية الحديثة فيستخدم نص لوريم ايبسوم فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. يتم استخدام نص لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ  للأحرف الموجودة في اللغة العربية بدلا من أو  عوضاً عن استخدام “هنا يوجد محتوى نصي، هنا يوجد محتوى نصي”  و تكرارها كثيرا 
                        في بعض المواقع الالكترونية الحديثة فيستخدم نص لوريم ايبسوم فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء.";

$args_description_posts=array(
	                      'post_type' =>'product',
	                      'posts_per_page'=>'-1',
	                     );
*/
/*$change_description=new WP_Query($args_description_posts);
if($change_description->have_posts()){
	while($change_description->have_posts()){
		$change_description->the_post();
		 wp_update_post( array('ID' => get_the_ID() , 'post_content' => $new_short_description ) );
	}
}*/