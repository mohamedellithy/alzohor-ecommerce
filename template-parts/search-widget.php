                        <?php 
                        if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
                                          <aside class="cell-3 left-shop">
                                                 <?php dynamic_sidebar( 'sidebar-1' ); ?>
                                          </aside>
                                            
                            <?php   endif; ?>
                        <div class="cell-9 show-all-products">
                           
                            <?php if( !is_single() || !is_singular() ): ?>
                                <div class="toolsBar">
                                    <div class="cell-10 left products-filter-top">

                                        <div class="left">
                                            <form class="form_order" method="POST">
                                                <span> ترتيب ب </span>
                                                <select name="order_by" class="order_by" >
                                                    <option value="date"  <?php echo ( ( !empty( $_GET['order_by'] ) && ($_GET['order_by']=='date') )?'selected':''); ?> > التاريخ </option>
                                                    <option value="price" <?php echo ( ( !empty( $_GET['order_by'] ) && ($_GET['order_by']=='price') )?'selected':''); ?> > السعر</option>
                                                    <option value="all"   <?php echo ( ( !empty( $_GET['order_by'] ) && ($_GET['order_by']=='all') )?'selected':''); ?> > الكل </option>
                                                </select>
                                            </form>

                                        </div>
                                        <div class="left">
                                            <form class="form_count_products" method="POST">
                                                <span> عدد المنتجات : </span>
                                                <select name="number_product" class="number_product">
                                                    <option value="20" <?php echo ( ( !empty( $_GET['number_product'] ) && ($_GET['number_product']=='20') )?'selected':''); ?> >20</option>
                                                    <option value="30" <?php echo ( ( !empty( $_GET['number_product'] ) && ($_GET['number_product']=='30') )?'selected':''); ?> >30</option>
                                                    <option value="50" <?php echo ( ( !empty( $_GET['number_product'] ) && ($_GET['number_product']=='50') )?'selected':''); ?> >50</option>
                                                    <option value="all" <?php echo ( ( !empty( $_GET['number_product'] ) && ($_GET['number_product']=='all') )?'selected':''); ?> >All</option>
                                                </select>
                                            </form>
                                        </div>
                                        <div class="left order-asc">
                                          <form  class="form_order_arrange_products" method="POST">
                                            <input name="order_product_arrange" class="order_product_arrange" type="hidden" value="<?php echo ( ( !empty( $_GET['order']  ) )?$_GET['order']:'asc'); ?>"/>  
                                            <a href="#">
                                                <i class="order_arrange  <?php echo ( ( !empty( $_GET['order'] ) && ($_GET['order']=='desc') )?'fa fa-sort-amount-desc':'fa fa-sort-amount-asc'); ?> "></i>
                                            </a>
                                          </form>
                                        </div>
                                    </div>
                                    <div class="right cell-2 list-grid">
                                        <a class="list-btn" href="#" data-title="List view" data-tooltip="true">
                                            <i class="fa fa-list"></i>
                                        </a>
                                        <a class="grid-btn selected" href="#" data-title="Grid view" data-tooltip="true">
                                            <i class="fa fa-th"></i>
                                        </a>
                                    </div>
                            </div>
                        <?php endif; ?>